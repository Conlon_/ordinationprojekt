package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {

	private Dosis[] dosisArr = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel) {
		super(patient, laegemiddel, startDen, slutDen);

	}

	// skal rettes, sætter alle dosis til samme tidspunkt

	public void opretDosis(double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		Dosis dosis1 = new Dosis(LocalTime.of(06, 00), morgenAntal);
		Dosis dosis2 = new Dosis(LocalTime.NOON, middagAntal);
		Dosis dosis3 = new Dosis(LocalTime.of(18, 00), aftenAntal);
		Dosis dosis4 = new Dosis(LocalTime.MIDNIGHT, natAntal);

		dosisArr[0] = dosis1;
		dosisArr[1] = dosis2;
		dosisArr[2] = dosis3;
		dosisArr[3] = dosis4;

	}

	@Override
	public double samletDosis() {
		// XXX umiddelbart god

		return doegnDosis() * super.antalDage();
	}

	@Override
	/*
	 * OBS:
	 */
	public double doegnDosis() {
		// XXX umiddelbart god
		return dosisArr[0].getAntal() + dosisArr[1].getAntal() + dosisArr[2].getAntal() + dosisArr[3].getAntal();
	}

	@Override
	public String getType() {
		// XXX umiddelbart god
		return "Daglig fast";
	}

	public Dosis[] getDoser() {
		return dosisArr;
	}

}
