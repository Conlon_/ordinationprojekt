package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligSkævTest {
	Patient p1, p2;
	Laegemiddel l1, l2;
	LocalDate LD1, LD2;

	@Before
	public void setUp() throws Exception {
		p1 = new Patient("022002-2020", "Lars", 60);
		p2 = new Patient("033003-3030", "Poul", 110);

		l1 = new Laegemiddel("Paracetamol", 2, 4, 6, "styk");
		l2 = new Laegemiddel("Spisepindepille", 0.2, 0.7, 1.4, "mg");

		LD1 = LocalDate.of(21, 01, 01);
		LD2 = LocalDate.of(21, 01, 10);

	}

	@Test
	public void testDagligSkaev() {
		DagligSkaev DST = new DagligSkaev(LD1, LD2, p2, l2);
		assertNotNull(DST);
		assertEquals(l2, DST.getLaegemiddel());
		assertTrue(LD1.isBefore(LD2));

	}

	@Test
	public void testOpretDosis() {
		DagligSkaev DS = new DagligSkaev(LD1, LD2, p1, l1);
		assertNotNull(DS);
		assertTrue(LD1.isBefore(LD2));
		assertEquals(0, DS.getDoser().size(), 0.1);
		DS.opretDosis(LocalTime.of(10, 11), 20.3);
		Dosis dosis = DS.getDoser().get(0);
		assertNotNull(DS.getDoser().get(0));
		assertEquals(20.3, dosis.getAntal(), 0.1);
		assertEquals(LocalTime.of(10, 11), dosis.getTid());

	}

	@Test
	public void testDoegnDosis() {
		DagligSkaev DS = new DagligSkaev(LD1, LD2, p1, l1);
		DS.opretDosis(LocalTime.of(1, 2), 2);
		DS.opretDosis(LocalTime.of(3, 4), 3);
		DS.opretDosis(LocalTime.of(5, 6), 4);
		DS.opretDosis(LocalTime.of(7, 8), 5);
		assertEquals(1.4, DS.doegnDosis(), 0.1);

	}

	@Test
	public void testSamletDosis() {
		DagligSkaev DS = new DagligSkaev(LD1, LD2, p1, l1);
		DS.opretDosis(LocalTime.of(1, 2), 2);
		DS.opretDosis(LocalTime.of(3, 4), 3);
		DS.opretDosis(LocalTime.of(5, 6), 4);
		DS.opretDosis(LocalTime.of(7, 8), 5);
		assertEquals(14, DS.samletDosis(), 0.1);
	}

}
