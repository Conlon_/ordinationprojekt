package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {
	private double antalEnheder;
	private ArrayList<LocalDate> dosisList = new ArrayList<>();

	public PN(Patient patient, Laegemiddel laegemiddel, LocalDate startDen, LocalDate slutDen, double antal) {
		super(patient, laegemiddel, startDen, slutDen);
		this.antalEnheder = antal;
		// TODO Auto-generated constructor stub
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		// XXX
		if (((givesDen.isBefore(super.getSlutDen())) && givesDen.isAfter(super.getStartDen()))) {

			dosisList.add(givesDen);
			return true;
		}

		return false;
	}

	public double doegnDosis() {
		// XXX
		double dosis = (getAntalGangeGivet() * getAntalEnheder())
				/ (ChronoUnit.DAYS.between(super.getStartDen(), super.getSlutDen()) + 1);
		return dosis;
	}

	public double samletDosis() {
		double sum = 0;
		for (LocalDate localDate : dosisList) {
			sum++;
		}
		return sum;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {

		int count = 0;
		if (dosisList.size() > 0) {
			for (LocalDate localDate : dosisList) {
				if (localDate.isBefore(LocalDate.now())) {
					count++;
				}
			}

		}

		return count;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	public ArrayList<LocalDate> getDosisList() {
		return dosisList;
	}

	@Override
	public String getType() {

		return "PN";
	}

}
