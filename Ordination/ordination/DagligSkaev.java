package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

	private ArrayList<Dosis> dosisList = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel) {
		super(patient, laegemiddel, startDen, slutDen);

	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		if (!dosisList.contains(dosis)) {
			dosisList.add(dosis);
		}
	}

	public ArrayList<Dosis> getDoser() {
		return dosisList;
	}

	// nedarvede metoder

	@Override
	public double samletDosis() {
		// XXX umiddelbart god
		return doegnDosis() * super.antalDage();
	}

	@Override
	public double doegnDosis() {
		// XXX umiddelbart god
		double sum = 0;
		for (Dosis dosis : dosisList) {
			sum += dosis.getAntal();
		}
		return sum / super.antalDage();
	}

	@Override
	public String getType() {
		// XXX umiddelbart god
		return "Daglig Skæv";
	}
}
