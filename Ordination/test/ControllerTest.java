package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Patient;
import storage.Storage;

public class ControllerTest {
	Controller controller = Controller.getController();
	Laegemiddel acetyl, para, fuci, laegemiddel;
	Patient jens, peter, ole;
	Storage storage;
	LocalDate dato;
	Patient patient;
	
	

	@Before
	public void setUp() throws Exception {
	peter = new Patient("10.10.2019-2222", "Peter Olsen", 22);
	jens = new Patient("10.10.1979-2222", "Jens Peter", 70);
	ole = new Patient("10.10.1979-2222", "Ole Jensen", 130);
	
	acetyl =  new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	para = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
	fuci = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
	
	controller = Controller.getTestController();

	patient = controller.opretPatient("123456-7890", "Dorthe Jensen", 75.5);
	laegemiddel = controller.opretLaegemiddel("Acetylsalicylsyre", 0.05, 0.10, 0.15, "Styk");
	dato = LocalDate.of(2021, 3, 8);
	}

	@Test
	public void anbefaletDosisPrDøgnTEST() {
		assertEquals(0.1, controller.anbefaletDosisPrDoegn(peter, acetyl),0.001);
		assertEquals(0.15, controller.anbefaletDosisPrDoegn(jens, acetyl),0.001);
		assertEquals(0.16, controller.anbefaletDosisPrDoegn(ole, acetyl),0.001);
	}

	
	@Before
	public void setup() {

		
	}

	@Test
	public void TC1testantalOrdinationerPrVægtprLægemiddel() {
		Laegemiddel para = controller.opretLaegemiddel("Paracetamol", 0.05, 0.10, 0.15, "Styk");
		Patient p1 = controller.opretPatient("123456-7890", "Dorthe Jensen", 75.5);
		Patient p2 = controller.opretPatient("123456-7890", "Jens Pedersen", 80.5);
		DagligFast dg1 = new DagligFast(dato, dato, p1, laegemiddel);
		DagligFast dg2 = new DagligFast(dato, dato, p2, laegemiddel);

		dg1.opretDosis(1, 1, 1, 1);
		dg2.opretDosis(1, 1, 1, 1);

		int antalordinationer = controller.antalOrdinationerPrVægtPrLægemiddel(50, 100, para);
		assertEquals(0, antalordinationer, 0);
	}

		@Test
		public void TC2testantalOrdinationerPrVægtprLægemiddel() {
			Patient p1 = controller.opretPatient("123456-7890", "Dorthe Jensen", 75.5);
			Patient p2 = controller.opretPatient("123456-7890", "Jens Pedersen", 80.5);
			DagligFast dg1 = new DagligFast(dato, dato, p1, laegemiddel);
			DagligFast dg2 = new DagligFast(dato, dato, p2, laegemiddel);
	
			dg1.opretDosis(1, 1, 1, 1);
			dg2.opretDosis(1, 1, 1, 1);
	
			int antalordinationer = controller.antalOrdinationerPrVægtPrLægemiddel(50, 100, laegemiddel);
			assertEquals(2, antalordinationer, 0);
		}

}


