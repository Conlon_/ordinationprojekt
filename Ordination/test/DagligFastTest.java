package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligFastTest {
	Patient p1, p2;
	Laegemiddel l1, l2;
	LocalDate LD1, LD2;

	@Before
	public void setUp() throws Exception {
		p1 = new Patient("022002-2020", "Lars", 60);
		p2 = new Patient("033003-3030", "Poul", 110);

		l1 = new Laegemiddel("Paracetamol", 2, 4, 6, "styk");
		l2 = new Laegemiddel("Spisepindepille", 0.2, 0.7, 1.4, "mg");

		LD1 = LocalDate.of(21, 01, 01);
		LD2 = LocalDate.of(21, 01, 10);

	}

	@Test
	public void testDagligFast() {
		DagligFast DFT = new DagligFast(LD1, LD2, p2, l2);
		assertNotNull(DFT);
		assertEquals(l2, DFT.getLaegemiddel());
		assertTrue(LD1.isBefore(LD2));

	}

	@Test
	public void testOpretDosis() {
		DagligFast DF = new DagligFast(LD1, LD2, p1, l1);
		assertNotNull(DF);
		assertTrue(LD1.isBefore(LD2));
		assertEquals(4, DF.getDoser().length, 0.1);
		DF.opretDosis(2, 2, 2, 2);
		assertNotNull(DF.getDoser());

	}

	@Test
	public void testDoegnDosis() {
		DagligFast DF = new DagligFast(LD1, LD2, p1, l1);
		DF.opretDosis(2, 2, 2, 2);
		assertEquals(8, DF.doegnDosis(), 0.1);

	}

	@Test
	public void testSamletDosis() {
		DagligFast DF = new DagligFast(LD1, LD2, p1, l1);
		DF.opretDosis(2, 2, 2, 2);
		assertEquals(80, DF.samletDosis(), 0.1);
	}

}
