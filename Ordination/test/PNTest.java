package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class PNTest {
	Patient p1, p2;
	Laegemiddel l1, l2;
	LocalDate LD1, LD2, LD3;

	@Before
	public void setUp() throws Exception {
		p1 = new Patient("022002-2020", "Lars", 60);
		p2 = new Patient("033003-3030", "Poul", 110);

		l1 = new Laegemiddel("Paracetamol", 2, 4, 6, "styk");
		l2 = new Laegemiddel("Spisepindepille", 0.2, 0.7, 1.4, "mg");

		LD1 = LocalDate.of(21, 01, 01);
		LD2 = LocalDate.of(21, 01, 10);
		LD3 = LocalDate.of(21, 01, 03);

	}

	@Test
	public void PNConstructTest() {
		PN pn = new PN(p1, l1, LD1, LD2, 20);
		assertNotNull(pn);
		assertTrue(LD1.isBefore(LD2));
		assertEquals(20, pn.getAntalEnheder(), 0.1);

	}

	@Test
	public void testGivDosis() {
		PN pn = new PN(p1, l1, LD1, LD2, 20);
		pn.givDosis(LD3);
		assertEquals(LD3, pn.getDosisList().get(0));

	}

	@Test
	public void testDoegnDosis() {
		PN pn = new PN(p1, l1, LD1, LD2, 10);
		pn.givDosis(LocalDate.of(21, 01, 03));
		pn.givDosis(LocalDate.of(21, 01, 04));
		pn.givDosis(LocalDate.of(21, 01, 05));
		pn.givDosis(LocalDate.of(21, 01, 06));
		pn.givDosis(LocalDate.of(21, 01, 06));
		assertEquals(5, pn.doegnDosis(), 0.1);
	}

	@Test
	public void testSamletDosis() {
		PN pn = new PN(p1, l1, LD1, LD2, 10);
		pn.givDosis(LocalDate.of(21, 01, 03));
		pn.givDosis(LocalDate.of(21, 01, 04));
		pn.givDosis(LocalDate.of(21, 01, 05));
		pn.givDosis(LocalDate.of(21, 01, 06));
		pn.givDosis(LocalDate.of(21, 01, 06));
		assertEquals(5, pn.samletDosis(), 0.1);
	}

	@Test
	public void testGetAntalGangeGivet() {
		PN pn = new PN(p1, l1, LD1, LD2, 10);
		pn.givDosis(LocalDate.of(21, 01, 03));
		pn.givDosis(LocalDate.of(21, 01, 04));
		pn.givDosis(LocalDate.of(21, 01, 05));
		pn.givDosis(LocalDate.of(21, 01, 06));
		pn.givDosis(LocalDate.of(21, 01, 06));
		assertEquals(5, pn.getAntalGangeGivet(), 0.1);
	}

}
